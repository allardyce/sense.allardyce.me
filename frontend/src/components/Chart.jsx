import React, { useState, useEffect } from 'react';
// import { format } from 'date-fns';
import axios from 'axios';

import { ResponsiveLine } from '@nivo/line';

import Durations from './Durations';

const Chart = ({ from }) => {
    const [data, setData] = useState([]);
    const [duration, setDuration] = useState('1d');

    useEffect(() => {
        (async () => {
            const readings = [axios.post('/api/readings', { sense: 'temperature', from, duration }), axios.post('/api/readings', { sense: 'humidity', from, duration })];

            const [{ data: temperature }, { data: humidity }] = await Promise.all(readings);

            const data = [
                {
                    id: 'temperature',
                    data: temperature.map(reading => ({ x: new Date(reading.time), y: reading.value }))
                },
                {
                    id: 'humidity',
                    data: humidity.map(reading => ({ x: new Date(reading.time), y: reading.value }))
                }
            ];

            setData(data);
        })();
    }, [duration]);

    // const setDuration = value => {
    //     console.log('set duration', value);
    // };

    return (
        <div className="chart">
            <Durations duration={duration} setDuration={setDuration} />
            <ResponsiveLine
                colors={{ scheme: 'category10' }}
                margin={{ top: 20, right: 40, bottom: 20, left: 40 }}
                animate={true}
                curve="monotoneX"
                data={data}
                xScale={{
                    type: 'time',
                    format: 'native',
                    precision: 'minute'
                }}
                xFormat="time:%d %H:%M"
                yFormat={value => `${value}`}
                yScale={{
                    type: 'linear',
                    max: 100
                }}
                axisLeft={{
                    format: value => `${value}°C`
                }}
                axisRight={{
                    format: value => `${value}%`
                }}
                axisBottom={{
                    format: '%d %H:%M'
                }}
                pointSize={2}
                pointBorderWidth={1}
                useMesh={true}
            />
        </div>
    );
};

export default Chart;
