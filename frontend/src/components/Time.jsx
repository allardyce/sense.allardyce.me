import React, { useState, useEffect } from 'react';

import formatTimestamp from '../helpers/formatTimestamp';

import Stat from './Stat';

const Time = () => {
    const [date, setDate] = useState(formatTimestamp());

    useEffect(() => {
        setTimeout(() => {
            setDate(formatTimestamp());
        }, 1000);
    }, [date]);

    return <Stat title="Time" value={date} />;
};

export default Time;
