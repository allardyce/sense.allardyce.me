import React from 'react';

const Stat = ({ title, value, symbol, description }) => {
    return (
        <div className="stat">
            <div className="stat-title">{title}</div>
            <div className="stat-value">
                {value}
                {symbol}
            </div>
            <div className="stat-description">&nbsp;{description}</div>
        </div>
    );
};

export default Stat;
