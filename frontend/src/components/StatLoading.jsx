import React from 'react';

const StatLoading = () => {
    return (
        <div className="stat is-loading">
            <div className="stat-title">&nbsp;</div>
            <div className="stat-value">&nbsp;</div>
            <div className="stat-description">&nbsp;</div>
        </div>
    );
};

export default StatLoading;
