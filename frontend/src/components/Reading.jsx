import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { format } from 'timeago.js';

import useInterval from '../hooks/useInterval';
import formatTimestamp from '../helpers/formatTimestamp';

import Stat from './Stat';
import StatLoading from './StatLoading';

const Reading = ({ title, path, sense, from, duration, symbol }) => {
    const [time, setTime] = useState(null);
    const [timeAgo, setTimeAgo] = useState('');
    const [timestamp, setTimestamp] = useState('');
    const [value, setValue] = useState(null);

    // Load in data
    useEffect(() => {
        (async () => {
            const { data } = await axios.post(path, { sense, duration, from });

            let datetime = new Date(data.time);
            setTime(datetime);
            setTimeAgo(format(datetime));
            setTimestamp(formatTimestamp(datetime));

            setValue(data.value);
        })();
    }, [path, sense]);

    // Update time ago formatted time
    useInterval(() => {
        if (time) setTimeAgo(format(time));
    }, 1000);

    if (!time || !value) return <StatLoading />;

    return <Stat title={title} symbol={symbol} value={value} description={`Last updated at: ${timestamp} (${timeAgo})`} />;
};

export default Reading;
