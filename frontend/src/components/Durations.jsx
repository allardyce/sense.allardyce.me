import React from 'react';

const Durations = ({ duration, setDuration }) => {
    const durations = [
        { label: '1 hour', value: '1h' },
        { label: '6 hours', value: '6h' },
        { label: '12 hours', value: '12h' },
        { label: '1 day', value: '1d' },
        { label: '1 week', value: '1w' }
    ];

    return (
        <div className="durations">
            {durations.map((d, i) => (
                <div className={'duration ' + (duration === d.value ? 'active' : '')} key={i} onClick={() => setDuration(d.value)}>
                    {d.label}
                </div>
            ))}
        </div>
    );
};

export default Durations;
