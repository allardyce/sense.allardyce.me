const formatTimestamp = (date = new Date()) => {
    let h = `${date.getHours()}`.padStart(2, '0');
    let m = `${date.getMinutes()}`.padStart(2, '0');
    let s = `${date.getSeconds()}`.padStart(2, '0');
    return `${h}:${m}:${s}`;
};

export default formatTimestamp;
