import { configureStore } from '@reduxjs/toolkit';

import { reducer as senseReducer } from './sense';

const store = configureStore({
    reducer: {
        sense: senseReducer
    }
});

console.log('Initial state: ', store.getState());

export default store;
