import { createSlice } from '@reduxjs/toolkit';
import axios from 'axios';

const sense = createSlice({
    name: 'sense',

    initialState: {
        temperature: [],

        humidity: [],

        highest: {
            temperature: {},
            humidity: {}
        }
    },

    reducers: {
        setLatestReadings(state, { payload }) {
            let [time, humidity, temperature] = payload.values[0];

            state.temperature.push({ time, value: temperature });
            state.humidity.push({ time, value: humidity });

            if (temperature > state.highest.temperature.value) {
                state.highest.temperature.time = time;
                state.highest.temperature.value = temperature;
            }

            if (humidity > state.highest.humidity.value) {
                state.highest.humidity.time = time;
                state.highest.humidity.value = humidity;
            }

            return state;
        },

        setHighestReading(state, { payload }) {
            let { label, time, value } = payload;
            state.highest[label].time = time;
            state.highest[label].value = value;
            return state;
        }
    }
});

const { setLatestReadings, setHighestReading } = sense.actions;

export const fetchLatestReadings = () => async dispatch => {
    const { data } = await axios.get('/api/reading/latest');
    dispatch(setLatestReadings(data));
};

export const fetchHighestReadings = () => async dispatch => {
    const { data } = await axios.get('/api/reading/highest/day');

    dispatch(setHighestReading(data.humidity));
    dispatch(setHighestReading(data.temperature));
};

export const { actions, reducer } = sense;

// export const { increment, decrement } = actions;

// export default counter;
