import React from 'react';

import Time from './components/Time';
import Reading from './components/Reading';
import Chart from './components/Chart';

const App = () => {
    return (
        <div className="App">
            <div className="row">
                <article className="message is-danger">
                    <div className="message-body">
                        Due to travelling, the raspberry pi sensor is currently offline. The site has been hard-code dated to when the sensor was giving readings. These values will not update until the raspberry pi is plugged back in.
                    </div>
                </article>
            </div>
            <div className="row">
                <div className="columns">
                    <div className="column">
                        <Time />
                    </div>

                    <div className="column">
                        <Reading path="/api/reading" sense="temperature" title="Current Temperature" symbol="°C" />
                    </div>

                    <div className="column">
                        <Reading path="/api/reading" sense="humidity" title="Current Humidity" symbol="%" />
                    </div>
                </div>
            </div>
            <div className="row">
                <div className="columns">
                    <div className="column">
                        <Reading path="/api/reading/max" sense="temperature" from="'2020-03-07T00:00:00Z'" duration="- 1w" title="Highest Temperature" symbol="°C" />
                    </div>

                    <div className="column">
                        <Reading path="/api/reading/max" sense="humidity" from="'2020-03-07T00:00:00Z'" duration="- 1w" title="Highest Humidity" symbol="%" />
                    </div>
                </div>
            </div>
            <div className="row">
                <Chart sense="temperature" from="'2020-03-07T00:00:00Z'" symbol="°C" />
            </div>
        </div>
    );
};

export default App;
