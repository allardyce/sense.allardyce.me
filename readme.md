## Developing

Prerequisites: docker, docker-compose, golang, git

1. Start the database:

    1.1 `cd db`

    1.2 `docker-compose up -d`

    2. Play: `docker exec -it influxdb.allardyce.me influx`

2. Start the frontend

    2.1 `cd frontend`

    2.2 `yarn start`

3. Start the server

    2.1 `cd server`

    2.2 `go run .`

## Running with Docker locally

1. Build docker image: `docker-compose -f docker-compose.dev.yml up -d`
