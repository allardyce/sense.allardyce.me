# build react frontend
FROM node:11-alpine as build-frontend

WORKDIR /app

COPY ./frontend/package.json .
COPY ./frontend/yarn.lock .

RUN yarn install

COPY ./frontend .

RUN yarn build

################################################################################################3

# build go server
FROM golang:1.13-alpine as build-server

RUN apk update && apk add --no-cache git

# Create appuser.
ENV USER=appuser
ENV UID=10001

# See https://stackoverflow.com/a/55757473/12429735RUN 
RUN adduser \
    --disabled-password \
    --gecos "" \
    --home "/nonexistent" \
    --shell "/sbin/nologin" \
    --no-create-home \
    --uid "${UID}" \
    "${USER}"

WORKDIR /go/src/app

COPY server .

RUN go get -d -v

# build executable
# RUN go build -o /go/bin/server .
# RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -a -installsuffix cgo -ldflags="-w -s" -o /go/bin/server .
RUN GOOS=linux GOARCH=amd64 go build -ldflags="-w -s" -o /go/bin/server

################################################################################################3

# production image
FROM alpine

# Import the user and group files from the builder.
COPY --from=build-server /etc/passwd /etc/passwd
COPY --from=build-server /etc/group /etc/group

COPY --from=build-server /go/bin/server /go/bin/server

COPY --from=build-frontend /app/build /public

# Use an unprivileged user.
USER appuser:appuser

EXPOSE 8080

ENTRYPOINT ["/go/bin/server"]