# Pi

Python 3 is used to read the DHT22 sensor and post the readings to our MQTT server.

## Setup

1. `sudo pip install --no-cache-dir -r requirements.txt`

2. `/home/pi/projects/pi/src/start.sh`

## Setup on Boot

1. sudo nano /etc/rc.local

2. Paste: `/home/pi/projects/pi/src/start.sh &`

## Stop

1. `/home/pi/projects/pi/src/stop.sh`

## Check if running

1. `jobs -l` or `ps xw`
