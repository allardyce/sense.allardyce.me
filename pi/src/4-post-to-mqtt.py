import time
import paho.mqtt.client as mqtt

MQTT_HOST = 'mqtt.allardyce.me'
MQTT_CLIENT = 'sense-01'
MQTT_USERNAME = 'sense'
MQTT_PASSWORD = '1KAtINH8IfgC2zM6Duko'


def on_connect(mqttc, obj, flags, rc):
    print("rc: " + str(rc))


def on_disconnect(client, userdata, rc):
    if rc != 0:
        print("Unexpected MQTT disconnection. Will auto-reconnect")
    print("client disconnected ok")


# MQTT Broker
mqttc = mqtt.Client(client_id = MQTT_CLIENT, clean_session = False)
mqttc.username_pw_set(username = MQTT_USERNAME, password = MQTT_PASSWORD)

mqttc.on_connect = on_connect
mqttc.on_disconnect = on_disconnect

mqttc.connect(MQTT_HOST, 1883)
mqttc.loop_start()

counter = 0

while True:
    mqttc.publish("sense/01/4-hello", "Hello World! {:d}".format(counter))
    counter += 1
    time.sleep(2.0)
