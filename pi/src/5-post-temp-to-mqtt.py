import time
import board
import adafruit_dht
import paho.mqtt.client as mqtt

MQTT_HOST = 'mqtt.allardyce.me'
MQTT_CLIENT = 'sense-01'
MQTT_USERNAME = 'sense'
MQTT_PASSWORD = '1KAtINH8IfgC2zM6Duko'


def on_connect(mqttc, obj, flags, rc):
    print("rc: " + str(rc))


def on_disconnect(client, userdata, rc):
    if rc != 0:
        print("Unexpected MQTT disconnection. Will auto-reconnect")
    print("client disconnected ok")


# MQTT Broker
mqttc = mqtt.Client(client_id=MQTT_CLIENT, clean_session=False)
mqttc.username_pw_set(username=MQTT_USERNAME, password=MQTT_PASSWORD)

mqttc.on_connect = on_connect
mqttc.on_disconnect = on_disconnect

mqttc.connect(MQTT_HOST, 1883)
mqttc.loop_start()

counter = 0

# Sensor - DHT22
sensor = adafruit_dht.DHT22(board.D4)


# Main Loop

while True:

    try:

        # Read the values
        ctime = time.time()
        temperature = sensor.temperature
        humidity = sensor.humidity

        # Print to console and publish to MQTT
        mqttc.publish("sense/01/reading",
                      "{},{:.1f},{}".format(ctime, temperature, humidity))
        print("Time: {}, Temp: {:.1f} C,  Humidity: {}% ".format(
            ctime, temperature, humidity))

        # Sleep for a bit
        time.sleep(30.0)

    except RuntimeError as error:
        # Errors happen fairly often, DHT's are hard to read, just keep going
        print(error.args[0])
