package main

import (
	"log"
	"os"
	"path"
)

// OpenLogFile - Creates and opens a log file
func OpenLogFile(logfile string) {
	if logfile != "" {

		os.MkdirAll(path.Dir(logfile), os.ModePerm)

		lf, err := os.OpenFile(logfile, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)

		if err != nil {
			log.Fatal("OpenLogfile: os.OpenFile:", err)
		}

		log.SetOutput(lf)
	}
}
