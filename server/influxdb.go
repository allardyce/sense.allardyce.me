package main

import (
	"fmt"
	"log"
	"os"
	"time"

	_ "github.com/influxdata/influxdb1-client" // this is important because of the bug in go mod
	client "github.com/influxdata/influxdb1-client/v2"
)

var c client.Client

// Influxdb - Setup database paramenters
func Influxdb() {

	host := os.Getenv("DB_HOST")

	var err error

	c, err = client.NewHTTPClient(client.HTTPConfig{Addr: host})

	if err != nil {
		log.Fatal(err)
	}

}

// Write - save time, temperature and humidity to database
func Write(t time.Time, temperature float64, humidity float64) {

	// Create a new point batch
	bp, _ := client.NewBatchPoints(client.BatchPointsConfig{
		Database:  "sense",
		Precision: "ms",
	})

	// Create a point and add to batch
	tags := map[string]string{
		"location": "carnarvon",
	}
	fields := map[string]interface{}{
		"temperature": temperature,
		"humidity":    humidity,
	}
	pt, err := client.NewPoint("reading", tags, fields, t)
	if err != nil {
		fmt.Println("Error: ", err.Error())
	}

	bp.AddPoint(pt)

	// Write the batch
	err = c.Write(bp)

	if err != nil {
		fmt.Println("[INFLUXDB-ERROR]: ", err.Error())
	}

	fmt.Println("Written to database:", t, temperature, humidity)

}

func setup() {

}

// Query - query the db
func Query(query string) (res []client.Result, err error) {

	q := client.NewQuery(query, "sense", "ms")

	if response, err := c.Query(q); err == nil && response.Error() == nil {
		res = response.Results
	} else {
		return res, err
	}
	return res, nil
}
