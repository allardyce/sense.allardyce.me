package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/gorilla/mux"
)

func root(w http.ResponseWriter, r *http.Request) error {
	w.Header().Set("Content-Type", "application/json")

	data := map[string]interface{}{"success": true, "time": time.Now()}

	json.NewEncoder(w).Encode(data)

	return nil

}

// Reading - a single sense reading
type Reading struct {
	Time  json.Number `json:"time"`
	Value json.Number `json:"value"`
}

func reading(w http.ResponseWriter, r *http.Request) error {

	type body struct {
		Sense string
	}
	var b body

	err := json.NewDecoder(r.Body).Decode(&b)
	if err != nil {
		return NewHTTPError(err, 400, "Failed to parse body.")
	}

	if b.Sense != "temperature" && b.Sense != "humidity" {
		return NewHTTPError(nil, 400, "Parameter 'sense' invalid. Required: 'temperature', 'humidity'")
	}

	res, err := Query(fmt.Sprintf("SELECT last(%s) FROM reading;", b.Sense))
	if err != nil {
		log.Println(err)
	}

	fmt.Println(res[0].Series[0].Values[0])

	if len(res) == 0 || len(res[0].Series) == 0 || len(res[0].Series[0].Values) == 0 {
		return NewHTTPError(nil, 400, "No readings found.")
	}

	latest := Reading{
		Time:  res[0].Series[0].Values[0][0].(json.Number),
		Value: res[0].Series[0].Values[0][1].(json.Number),
	}

	json.NewEncoder(w).Encode(latest)

	return nil

}

func readings(w http.ResponseWriter, r *http.Request) error {

	type body struct {
		Sense    string
		From     string
		Duration string
	}
	var b body

	err := json.NewDecoder(r.Body).Decode(&b)
	if err != nil {
		return NewHTTPError(err, 400, "Failed to parse body.")
	}

	if b.Sense != "temperature" && b.Sense != "humidity" {
		return NewHTTPError(nil, 400, "Parameter 'sense' invalid. Required: 'temperature', 'humidity'")
	}

	if b.From == "" {
		b.From = "now()"
	}

	var query string

	if b.Duration == "1h" {
		query = fmt.Sprintf("SELECT max(%s) FROM reading WHERE time > %s - 1h GROUP BY time(1m) fill(none);", b.Sense, b.From)
	}
	if b.Duration == "6h" {
		query = fmt.Sprintf("SELECT max(%s) FROM reading WHERE time > %s - 6h GROUP BY time(6m) fill(none);", b.Sense, b.From)
	}
	if b.Duration == "12h" {
		query = fmt.Sprintf("SELECT max(%s) FROM reading WHERE time > %s - 12h GROUP BY time(8m) fill(none);", b.Sense, b.From)
	}
	if b.Duration == "1d" {
		query = fmt.Sprintf("SELECT max(%s) FROM reading WHERE time > %s - 1d GROUP BY time(10m) fill(none);", b.Sense, b.From)
	}
	if b.Duration == "1w" {
		query = fmt.Sprintf("SELECT max(%s) FROM reading WHERE time > %s - 1w GROUP BY time(1h) fill(none);", b.Sense, b.From)
	}

	fmt.Println(query)

	res, err := Query(query)
	if err != nil {
		log.Println(err)
	}

	var readings []Reading

	if len(res) == 0 || len(res[0].Series) == 0 || len(res[0].Series[0].Values) == 0 {
		w.Write([]byte(`[]`))
		return nil
	}

	for i := 0; i < len(res[0].Series[0].Values); i++ {
		readings = append(readings, Reading{
			Time:  res[0].Series[0].Values[i][0].(json.Number),
			Value: res[0].Series[0].Values[i][1].(json.Number),
		})
	}
	json.NewEncoder(w).Encode(readings)

	return nil

}

func max(w http.ResponseWriter, r *http.Request) error {

	type body struct {
		Sense    string
		From     string
		Duration string
	}
	var b body

	err := json.NewDecoder(r.Body).Decode(&b)
	if err != nil {
		return NewHTTPError(err, 400, "Failed to parse body.")
	}

	if b.Sense != "temperature" && b.Sense != "humidity" {
		return NewHTTPError(nil, 400, "Parameter 'sense' invalid. Required: 'temperature', 'humidity'")
	}

	if b.From == "" {
		b.From = "now()"
	}

	if b.Duration == "" {
		b.Duration = "- 1d"
	}

	res, err := Query(fmt.Sprintf("SELECT max(%s) FROM reading WHERE time > %s %s;", b.Sense, b.From, b.Duration))
	if err != nil {
		log.Println(err)
	}

	if len(res) == 0 || len(res[0].Series) == 0 || len(res[0].Series[0].Values) == 0 {
		return NewHTTPError(nil, 400, "No readings found.")
	}

	record := Reading{
		Time:  res[0].Series[0].Values[0][0].(json.Number),
		Value: res[0].Series[0].Values[0][1].(json.Number),
	}

	json.NewEncoder(w).Encode(record)

	return nil

}

// HTTP - Starts the HTTP part of the server
func HTTP() {

	r := mux.NewRouter().StrictSlash(true)

	r.Handle("/api", RootHandler(root))
	r.Handle("/api/reading", RootHandler(reading)).Methods(http.MethodPost)
	r.Handle("/api/readings", RootHandler(readings)).Methods(http.MethodPost)
	r.Handle("/api/reading/max", RootHandler(max)).Methods(http.MethodPost)

	// websockets (socket.go)
	r.HandleFunc("/api/ws", Websockets)

	fs := http.FileServer(http.Dir("/public"))
	r.PathPrefix("/").Handler(fs)

	// Setup logging
	log.SetFlags(log.Ldate | log.Ltime | log.Lshortfile)

	fmt.Println("Starting HTTP module.")

	host := os.Getenv("WEB_HOST")

	log.Fatal(http.ListenAndServe(host, r))
	// log.Fatal(http.ListenAndServe("localhost:8080", logRequest(http.DefaultServeMux)))

	// http.Handle("/", r)

	// http.HandleFunc("/api", func(w http.ResponseWriter, r *http.Request) {

	// 	fmt.Println("/api called")

	// 	data := Root{Success: true, Time: time.Now()}

	// 	json.NewEncoder(w).Encode(data)

	// })

	// http.HandleFunc("/api/reading", func(w http.ResponseWriter, r *http.Request) {

	// 	res, err := Query("SELECT last(*) FROM reading;")
	// 	if err != nil {
	// 		log.Println(err)
	// 	}

	// 	json.NewEncoder(w).Encode(res[0].Series[0])

	// })

	// http.HandleFunc("/api/reading/latest", func(w http.ResponseWriter, r *http.Request) {

	// 	res, err := Query("SELECT last(*) FROM reading;")
	// 	if err != nil {
	// 		log.Println(err)
	// 	}

	// 	json.NewEncoder(w).Encode(res[0].Series[0])

	// })

	// http.HandleFunc("/api/reading/highest/day", func(w http.ResponseWriter, r *http.Request) {

	// 	res, err := Query("SELECT max(temperature) as max_temperature FROM reading WHERE time > now() - 1d; SELECT max(humidity) as max_humidity FROM reading WHERE time > now() - 1d;")
	// 	if err != nil {
	// 		log.Println(err)
	// 	}

	// 	payload := map[string]interface{}{
	// 		"temperature": map[string]interface{}{
	// 			"label": "temperature",
	// 			"time":  res[0].Series[0].Values[0][0].(json.Number),
	// 			"value": res[0].Series[0].Values[0][1].(json.Number),
	// 		},
	// 		"humidity": map[string]interface{}{
	// 			"label": "humidity",
	// 			"time":  res[1].Series[0].Values[0][0].(json.Number),
	// 			"value": res[1].Series[0].Values[0][1].(json.Number),
	// 		},
	// 	}

	// 	json.NewEncoder(w).Encode(payload)

	// })

	// http.HandleFunc("/api/reading/hour", func(w http.ResponseWriter, r *http.Request) {

	// 	res, err := Query("SELECT * FROM reading WHERE time > now() - 1h;")
	// 	if err != nil {
	// 		log.Println(err)
	// 	}

	// 	json.NewEncoder(w).Encode(res[0].Series[0])

	// })

	// http.HandleFunc("/api/reading/day", func(w http.ResponseWriter, r *http.Request) {

	// 	res, err := Query("SELECT * FROM reading WHERE time > now() - 1d;")
	// 	if err != nil {
	// 		log.Println(err)
	// 	}

	// 	json.NewEncoder(w).Encode(res[0].Series[0])

	// })

}

func logRequest(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.Printf("%s %s %s\n", r.RemoteAddr, r.Method, r.URL)
		handler.ServeHTTP(w, r)
	})
}
