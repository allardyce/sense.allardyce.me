package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"time"
)

// Root - The root response structure
type Root struct {
	Success bool      `json:"success"`
	Time    time.Time `json:"time"`
}

// HTTP - Starts the HTTP part of the server
func HTTP() {

	http.HandleFunc("/api", func(w http.ResponseWriter, r *http.Request) {

		fmt.Println("/api called")

		data := Root{Success: true, Time: time.Now()}

		json.NewEncoder(w).Encode(data)

	})

	http.HandleFunc("/api/reading/latest", func(w http.ResponseWriter, r *http.Request) {

		res, err := Query("SELECT last(*) FROM reading;")
		if err != nil {
			log.Println(err)
		}

		json.NewEncoder(w).Encode(res[0].Series[0])

	})

	http.HandleFunc("/api/reading/highest/day", func(w http.ResponseWriter, r *http.Request) {

		res, err := Query("SELECT max(temperature) as max_temperature FROM reading WHERE time > now() - 1d; SELECT max(humidity) as max_humidity FROM reading WHERE time > now() - 1d;")
		if err != nil {
			log.Println(err)
		}

		payload := map[string]interface{}{
			"temperature": map[string]interface{}{
				"label": "temperature",
				"time":  res[0].Series[0].Values[0][0].(json.Number),
				"value": res[0].Series[0].Values[0][1].(json.Number),
			},
			"humidity": map[string]interface{}{
				"label": "humidity",
				"time":  res[1].Series[0].Values[0][0].(json.Number),
				"value": res[1].Series[0].Values[0][1].(json.Number),
			},
		}

		json.NewEncoder(w).Encode(payload)

	})

	http.HandleFunc("/api/reading/hour", func(w http.ResponseWriter, r *http.Request) {

		res, err := Query("SELECT * FROM reading WHERE time > now() - 1h;")
		if err != nil {
			log.Println(err)
		}

		json.NewEncoder(w).Encode(res[0].Series[0])

	})

	http.HandleFunc("/api/reading/day", func(w http.ResponseWriter, r *http.Request) {

		res, err := Query("SELECT * FROM reading WHERE time > now() - 1d;")
		if err != nil {
			log.Println(err)
		}

		json.NewEncoder(w).Encode(res[0].Series[0])

	})

	fs := http.FileServer(http.Dir("/public"))
	http.Handle("/", fs)

	// Setup logging
	// OpenLogFile("./http.log")
	log.SetFlags(log.Ldate | log.Ltime | log.Lshortfile)

	fmt.Println("Starting HTTP module.")

	host := "0.0.0.0:8080"

	if os.Getenv("ENV") != "PRODUCTION" {
		host = "localhost:8080"
	}

	log.Fatal(http.ListenAndServe(host, logRequest(http.DefaultServeMux)))
	// log.Fatal(http.ListenAndServe("localhost:8080", logRequest(http.DefaultServeMux)))

}

func logRequest(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.Printf("%s %s %s\n", r.RemoteAddr, r.Method, r.URL)
		handler.ServeHTTP(w, r)
	})
}
