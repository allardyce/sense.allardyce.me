package main

import (
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
	"time"

	mqtt "github.com/eclipse/paho.mqtt.golang"
)

// MQTT - Connect to server and subscribe to sense topic
func MQTT() {

	// Setup logging
	// OpenLogFile("./mqtt.log")
	log.SetFlags(log.Ldate | log.Ltime | log.Lshortfile)

	host := os.Getenv("MQTT_HOST")
	clientID := os.Getenv("MQTT_CLIENT")
	username := os.Getenv("MQTT_USERNAME")
	password := os.Getenv("MQTT_PASSWORD")

	connOpts := mqtt.NewClientOptions().AddBroker("tcp://" + host + ":1883").SetClientID(clientID).SetCleanSession(true)

	connOpts.SetUsername(username)
	connOpts.SetPassword(password)

	connOpts.OnConnect = func(c mqtt.Client) {
		if token := c.Subscribe("sense/01/reading", 0, onMessageReceived); token.Wait() && token.Error() != nil {
			panic(token.Error())
		}
	}

	client := mqtt.NewClient(connOpts)

	if token := client.Connect(); token.Wait() && token.Error() != nil {
		panic(token.Error())
	} else {
		fmt.Printf("MQTT: Connected to %s\n", host)
		log.Printf("Connected to %s\n", host)
	}

}

func onMessageReceived(client mqtt.Client, message mqtt.Message) {
	fmt.Printf("Received message on topic: %s\nMessage: %s\n", message.Topic(), message.Payload())
	log.Printf("Topic: '%s'. Message: '%s'", message.Topic(), message.Payload())
	r := strings.Split(string(message.Payload()), ",")
	temperature, humidty := r[1], r[2]

	// Convert from python time to golang time
	// 1580722630.1885753 -> seconds.nanoseconds -> go timedate
	times := strings.Split(r[0], ".")
	s, err := strconv.ParseInt(times[0], 10, 64)
	if err != nil {
		panic(err)
	}
	ns, err := strconv.ParseInt(times[1], 10, 64)
	if err != nil {
		panic(err)
	}
	t := time.Unix(s, ns)

	// Convert values to float64
	temperatureFloat, err := strconv.ParseFloat(temperature, 64)
	humidtyFloat, err := strconv.ParseFloat(humidty, 64)

	Write(t, temperatureFloat, humidtyFloat)
}
