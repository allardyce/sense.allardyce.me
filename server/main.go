package main

import (
	"fmt"
	"log"
	"os"

	"github.com/joho/godotenv"
)

// Hello - Prints a welcome to the world
func main() {

	if os.Getenv("ENV") != "PRODUCTION" {
		err := godotenv.Load()
		if err != nil {
			log.Fatal("Error loading .env file", err)
		}
	}

	go Influxdb()

	go HTTP()

	go MQTT()

	fmt.Println("All modules are a go.")

	select {}
}
