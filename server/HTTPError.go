package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
)

// HTTPError implements ClientError interface.
type HTTPError struct {
	Cause   error  `json:"-"`
	Message string `json:"message"`
	Status  int    `json:"-"`
}

// ClientError is an error whose details to be shared with client.
type ClientError interface {
	Error() string
	// ResponseBody returns response body.
	ResponseBody() ([]byte, error)

	GetStatus() int
}

func (e *HTTPError) Error() string {
	if e.Cause == nil {
		return e.Message
	}
	return e.Message + " : " + e.Cause.Error()
}

// ResponseBody returns JSON response body.
func (e *HTTPError) ResponseBody() ([]byte, error) {
	body, err := json.Marshal(e)
	if err != nil {
		return nil, fmt.Errorf("Error while parsing response body: %v", err)
	}
	return body, nil
}

// Returns the error status code
func (e *HTTPError) GetStatus() int {
	return e.Status
}

// Creates a new HTTPError
func NewHTTPError(err error, status int, message string) error {
	return &HTTPError{
		Cause:   err,
		Message: message,
		Status:  status,
	}
}

// Root handler
type RootHandler func(http.ResponseWriter, *http.Request) error

// rootHandler implements http.Handler interface.
func (fn RootHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {

	// Always json
	w.Header().Set("Content-Type", "application/json; charset=utf-8")

	err := fn(w, r) // Call handler function

	if err == nil {
		return
	}

	// Handle error

	log.Printf("[ERROR]: %v", err)

	// Check if it is a ClientError.
	clientError, ok := err.(ClientError)
	if !ok {
		// If the error is not ClientError, assume that it is ServerError.
		w.WriteHeader(500) // return 500 Internal Server Error.
		return
	}

	body, err := clientError.ResponseBody() // Try to get response body of ClientError.
	if err != nil {
		log.Printf("[ERROR]: %v", err)
		w.WriteHeader(500)
		return
	}

	w.WriteHeader(clientError.GetStatus())

	w.Write(body)
}
