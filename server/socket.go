package main

import (
	"fmt"
	"net/http"

	"github.com/gorilla/websocket"
)

// var conn *websocket.Conn

// Websockets - setup endpoint
func Websockets(w http.ResponseWriter, r *http.Request) {

	// var err error

	conn, err := websocket.Upgrade(w, r, w.Header(), 1024, 1024)

	fmt.Println("webscoket connected")
	fmt.Println(conn)

	if err != nil {
		http.Error(w, "Could not open websocket connection", http.StatusBadRequest)
	}

	// go echo(conn)

}

// type msg struct {
// 	Num int
// }

// func echo(conn *websocket.Conn) {
// 	for {
// 		m := msg{}

// 		err := conn.ReadJSON(&m)
// 		if err != nil {
// 			fmt.Println("Error reading json.", err)
// 		}

// 		fmt.Printf("Got message: %#v\n", m)

// 		if err = conn.WriteJSON(m); err != nil {
// 			fmt.Println(err)
// 		}
// 	}
// }

// func broadcast(message string) {

// 	fmt.Println(conn)

// 	err := conn.WriteJSON(message)

// 	if err != nil {
// 		fmt.Println(err)
// 	}
// }
